﻿using System;
using TechTalk.SpecFlow;
using BookLibrary;
using TechTalk.SpecFlow.Assist;
using System.Collections.Generic;

namespace BookLibrary.Tests
{
    [Binding]
    public class LibrarySteps
    {
        Book particularBook = new Book();
        LibraryServices services = new LibraryServices();
        String bookName, bookAuthor;
        int bookPrice;
        [Given(@"A book with the name ""(.*)""")]
        public void GivenABookWithTheName(string p0)
        {
            bookName = p0;
        }
        
        [Given(@"the price is ""(.*)""")]
        public void GivenThePriceIs(int p0)
        {
            bookPrice = p0;
        }
        
        [Given(@"Author is ""(.*)""")]
        public void GivenAuthorIs(string p0)
        {
            bookAuthor = p0;
        }
        
        [Given(@"Books are there in the library")]
        public void GivenBooksAreThereInTheLibrary()
        {
            
        }
        
        [When(@"I adde the book to the Library")]
        public void WhenIAddeTheBookToTheLibrary()
        {
            services.AddBook(bookName, bookPrice, bookAuthor);
        }
        
        [When(@"I fetch that book")]
        public void WhenIFetchThatBook()
        {
            particularBook = services.getBook(bookName);
        }
        
        [When(@"I fetch all book")]
        public void WhenIFetchAllBook()
        {
            
        }
        
        [Then(@"the library should contain a book like")]
        public void ThenTheLibraryShouldContainABookLike(Table table)
        {
            var books = services.GetAllBooks();
            table.CompareToSet(books);

        }
        [Then(@"the library should return a book like")]
        public void ThenTheLibraryShouldReturnABookLike(Table table)
        {
            table.CompareToInstance(particularBook);
        }


        [Then(@"the library should contain books like")]
        public void ThenTheLibraryShouldContainBooksLike(Table table)
        {
            var books = services.GetAllBooks();
            table.CompareToSet(books);
        }


        [When(@"I delete that book")]
        public void WhenIDeleteThatBook()
        {
            services.remove(bookName);
        }


        [BeforeScenario("getAllBooks")]
       public void AddingBooks()
        {
            services.AddBook("Shadow and Bone", 500, "Leigh Bardugo");
            services.AddBook("Harry Potter", 1000, "JK Rowling");
        }
        [BeforeScenario("getABook")]
        public void AddingBooks2()
        {
            services.AddBook("Shadow and Bone", 500, "Leigh Bardugo");
        }
        [BeforeScenario("deleteBook")]
        public void addBooks()
        {
            services.AddBook("Shadow and Bone", 500, "Leigh Bardugo");
            services.AddBook("Harry Potter", 1000, "JK Rowling");
        }
        public void delete()
        {
            services.remove(bookName);
        }

    }
}
