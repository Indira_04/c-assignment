﻿Feature: : Library
	A System to add, delete and fetch books from a library.

@addBook
Scenario: Adding a book to the library
	Given A book with the name "Shadow and Bone"
	And the price is "500"
	And Author is "Leigh Bardugo"
	When I adde the book to the Library
	Then the library should contain a book like
	| Name            | Price | Author        |
	| Shadow and Bone | 500   | Leigh Bardugo |

@getABook
Scenario: Fetching Particular book from the library
	Given A book with the name "Shadow and Bone"
	When I fetch that book
	Then the library should return a book like
	| Name            | Price | Author        |
	| Shadow and Bone | 500   | Leigh Bardugo |

@getAllBooks
Scenario: Fetching All books from the library
	Given Books are there in the library
	When I fetch all book
	Then the library should contain books like
	| Name            | Price | Author        |
	| Shadow and Bone | 500   | Leigh Bardugo |
	| Harry Potter    | 1000  | JK Rowling    |


@deleteBook
Scenario: Deleting a particular book from the library
	Given A book with the name "Harry Potter"
	When I delete that book
	Then the library should contain books like
	| Name            | Price | Author        |
	| Shadow and Bone | 500   | Leigh Bardugo |

