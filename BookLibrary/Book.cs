﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookLibrary
{
    public class Book
    {
        public string Name { get; set; }
        public int Price { get; set; }
        public string Author { get; set; }
    }
}
