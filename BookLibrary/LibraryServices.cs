﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookLibrary
{
    public class LibraryServices
    {
        List<Book> books = new List<Book>();

        public void AddBook(String name, int price, string author)
        {

            books.Add(
                new Book()
                {
                    Name = name,
                    Price = price,
                    Author = author
                }
            );
        }
        public void remove(String name)
        {
            var value = books.Find(
                    book => book.Name == name
                 );
            if (value != null)
            {
                books.Remove(value);
            }
            else
            {
                Console.WriteLine("Please Enter Valid a Book name");
            }

        }
        public Book getBook(string name)
        {
            var result = books.Find(book => book.Name == name);
            return result;

        }
        public List<Book> GetAllBooks()
        {
            return books;
        }
    }
}
